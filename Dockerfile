FROM apache/nifi:1.10.0

USER root

RUN set -x; \
	apt-get update \
	&& apt-get install -y --no-install-recommends \
	python3 \
	python3-pip \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*